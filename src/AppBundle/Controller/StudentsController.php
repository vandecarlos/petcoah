<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Students;


class StudentsController extends FOSRestController 
{
    /**
     * @Rest\Get("/Students")
     */
    public function getAction()
    {
      $restresult = $this->getDoctrine()->getRepository('AppBundle:Students')->findAll();
        if ($restresult === null) {
          return new View("there are no Studentss exist", Response::HTTP_NOT_FOUND);
     }
        return $restresult;
    }
	
	  /**
	 * @Rest\Get("/Students/{id}")
	 */
	 public function idAction($id)
	 {
	   $singleresult = $this->getDoctrine()->getRepository('AppBundle:Students')->find($id);
	   if ($singleresult === null) {
			return new View("Students not found", Response::HTTP_NOT_FOUND);
	   }
		return $singleresult;
	 }

	 /**
	 * @Rest\Post("/Students/")
	 */
	 public function postAction(Request $request)
	 {
		$data = new Students;
		$name = $request->get('name');
	 if(empty($name))
	 {
		return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
	 } 
		$data->setName($name);
		$em = $this->getDoctrine()->getManager();
		$em->persist($data);
		$em->flush();
		return new View("Students Added Successfully", Response::HTTP_OK);
	 }
		
		
	 /**
	 * @Rest\Put("/Students/{id}")
	 */
	 public function updateAction($id,Request $request)
	 { 
		$data = new Students;
		$name = $request->get('name');
		$sn = $this->getDoctrine()->getManager();
		$Students = $this->getDoctrine()->getRepository('AppBundle:Students')->find($id);
		if (empty($Students)) {
		   return new View("Students not found", Response::HTTP_NOT_FOUND);
		} 
		elseif(!empty($name)){
		   $Students->setName($name);
		   $sn->flush();
		   return new View("Students Updated Successfully", Response::HTTP_OK);
		}
		elseif(!empty($name)){
			$Students->setName($name);
			$sn->flush();
			return new View("Students Name Updated Successfully", Response::HTTP_OK); 
		}
		else return new View("Students name cannot be empty", Response::HTTP_NOT_ACCEPTABLE); 
	}	
		
	
} 
